General pattern of Future[Either[OError,B] for results, or Either[OError,B] if not blocking then is used in code.

EitherT[Future,OError,B] where OError is standard error type, which extends exception. 
EitherT[Future,OError,Json] as that is standard for calling REST services is our example.

> 
> The general case where EitherT[Future,OError,G[C]] where G is a collection list List, Vector or Option is also common.
> Section on nesting and functors pending.
> 
> Things to investigate: raiseError and handleError on EitherTErrorMonad and the recover instead of leftMap which is more clear.


Standard overkill imports in case things are not working and since we are working with Future it is usually necessary
to have an implicit execution context in scope.

```scala
import cats._
import cats.data._
import cats.implicits._


import scala.concurrent.ExecutionContext.Implicits.global

```

Since we are using Future, need to have an implicit execution context in scope, for default one:
import scala.concurrent.ExecutionContext.Implicits.global


## Constructing and Massaging Things into EitherT

So... either EitherT we want to "lift" all operations into Future[OError[B]] transformer.
Recall OError is type def for Either[MyError,B]
So it is really Future[Either[MyError,B]]

The standard shape of EitherT in the documentation if EitherT[F,A,B]
F and Either  must have Monads, Cats defined Monad for both already.
A and B do not need to be?

Few examples:

- EitherT(x) where x if a Future[Either[OError,B]] yields an EitherT[Future,OError,B]
- EitherT.fromEither(x) where x is type Either[A,B] or Either[OError,Json] in our case.
And then you can interpret the actual code too:

```scala
import cats._
import cats.data._
import cats.implicits._
def left[F[_], A, B](fa: F[A])(implicit F: Functor[F]): EitherT[F, A, B] = EitherT(F.map(fa)(Either.left))
def right[F[_], A, B](fb: F[B])(implicit F: Functor[F]): EitherT[F, A, B] = EitherT(F.map(fb)(Either.right))
def pure[F[_], A, B](b: B)(implicit F: Applicative[F]): EitherT[F, A, B] = right(F.pure(b))
```

LiftT is just an alias for right.

Building from option or either is simple, just remember to include Type classes. 


So, ommitting the type classes:
```scala

val x = correctAnswer
val y = OError("Houston we have a problem")
EitherT.left(y)   //works if type is Future[A] or Future[OError] in our case.
EitherT.right(x)  // works if type is Future[B] or Future[Json] in our case.
EitherT.pure(x)   // works if type is B or just Json in our case.
```



```scala
val ot = EitherT.fromOption(optVal, oError("Option was None"))
val et =  EitherT.fromEither[Future](massageB) where massageA is Either[OError,B]
val vt = EitherT.cond[Future](if (i.isOK), i, OError("i failed a precheck"))

```


```scala
val foo: Future[Either[A,B]] = ???
val bar: Future[Either[OError,B]] = ???
val fot = EitherT(foo)
val fot2 = EitherT(bar)
```


Using for comprehensions, which essentially invoke flatMap works well at higher level, and even better
if the functions called take
```scala
for {
    ans1 <- EitherT(callServiceA(rq)). where callServiceA is Future[Either[OError,Json]]
    ans2 <- EitherT.fromEither[Future](massageA) where massageA is Either[OError,Json]
    ...
    } yield (ans1, ansN)
```    
 



Anyway, the transformers are most interesting of course and the general naming on these is F[Either[A,B]] => F[Either[C,D]]


Not sure what a foreach equivalent is yet.

## Transforming an Existing EitherT

These are the basic operations which take a function to modify an EitherT value.
Let `f` denote the function and we stick with:

```scala
val a:EitherT[Future,OError,Json] // case corresponding to  
val general: EitherT[F,A,B]
val possibleOut: EitherT[Future, Alert, String]
val possibleOutcomes: EitherT[F, C, D]
```

### map -- Transform Success Value to Success Value

takes f:B=>D
e.g. val f(j:Json):String =  j.spaces2
et.map(f)

### leftMap -- Transform Error Value to Error Value
leftMap takes f: A=> C, context is:  EitherT[F,A,B] => EitherT[F,C,B]
In our case of EitherT[Future,OError,B] this means change the error message.

Not that when chaining EitherT often the A class needs to map, thus the standarization on OError


### flatMap -- Transform Success Value 
Via a asyncfunction that returns an EitherT basically.
Takes a function `f: (b:B) => EitherT[Future,OError,d:D]`


### flatMapF -- Transform Success Value
Same as flatmap, but `f` returns Future[Either[A,D]] or Future[Either[A,D]]

flatMapF takes B => Future[Either[AA,D]].  

Here AA is type of A  This is very handy if you keep your functions in Future[Either[A,B]] form.


### subflatMap -- Transform Success Value with Possible Error
This is used when `f` returns an `Either` because it may fail.

val f: (B)=> Either[AA,D]

### semiflatMap -- Transform Success Value with a Future function
This handles a Future function that doesn't return an Either. Typically used when should not fail.
Because it is a Future, it may always fail at that level of course.
val f: (B) = F[D]
e.g.  def fetchSomethingElse(j:Json): Address = Database.fetchAddr(j.idNumber)

semiflatMap is B => F[D], context is: EitherT[F,A,B] => EitherT[F,A,D]


##  Travesable Stuff
a) Never trust intellij highlighting, it will mess with your mind.
b) Not sure any reason to use traverse instead of traverseU which has better type binding.

#### sequenceU
if you have a val x = List[EitherT[F,A,B]] then x.sequenceU yields F[A,List[B]]
This is essentially folding on the Either with List.empty and combine as List + B
If second item in the list is error, you have already executed the N other things you are throwing away.

I am guessing that  A |+| A => A  (combine) must defined (to deal with Either) and the U is lifting B combine B
to G[B] |+| G[B], i.e. List[B] + List[B]

It needs at least an implicit: cats.Unapply[cats.Applicative, C]

And A needs to be in a Semigroip.

### Traversing

The classic Scala traverse and sequence on Future this is a handy way to execute N thinks concurrently..
```scala 
def getBalancesInParallel(accounts:Seq[AccountRecord]) = {
    val x: Future[Seq[Either[OError, XRP]]] =  Future.traverse(accounts)(v => getBalance(v).value)
  }

  def getBalance(a:AccountRecord): EitherT[Future, OError, XRP] =Actions.xrpBalance(AccountAddress(a.account))
```

That really gets us a meaningful result, Future[Seq[Either[OError, XRP]]] which makes sense.
Each balance is either an Error (can't get balance) or an XRP amount.
Traversing brings up the Future into our shape.
Seq[Future[Either[OError,XRP]]] might allow more aggresive downstream processing of each element though.

## Misc


### combine
If B is semigroup, e.g. List, then   x.combine(y) 
Semigroup defined |@| also.
So basically a combine on 2 EitherT[F,A,B] is just a combine on  2 Bs.


### Nested[....]

```scala
val res = Actions.xrpBalance(AccountAddress(a.account))
val xx: Nested[Future, ({type λ[β$2$] = Either[OError, β$2$]})#λ, XRP] = res.toNested
val xxx: Nested[Future, ({type λ[β$6$] = Validated[NonEmptyList[OError], β$6$]})#λ, XRP] = res.toNestedValidatedNel
val nested: Nested[Future, ({type λ[β$2$] = Either[OError, β$2$]})#λ, Seq[AccountRecord]] = sysAccounts.toNested


```
Nice types huh! This is based on the Kind-Project plugin so ugly.
But can we compose nested. Problem is that Functor and Nested tend to like F[_] not F[_,_] based on exampled.

Two problems with current code. Seq needs to be a List. The Nested is a known issue and use type OError = Either[A,B]


### tupleLeft and tupleRight
Often we have a pure value x and a f(x) => EitherT[Future,OError,x:X]
product/fproduct/mproduct are handy. But ran into case where we have a List[X] and f(x) => EitherT[...]
```scala
def f(x:X):EitherT[Future,OError,b:B]
val l:List[X]
va; answer = EitherT[Future,OError,List[(X,B)] = l.traverseU(x => f(x).tupleLeft(x))
```
Double check this from memory. Forget if started with EitherT[Future,OError,List[X]]
(Actually, had that case too, forget which I used tuple vs mproduct on.)

## Things we get based on Types

Recall the main types
* Monoid -- empty for map
* Applicative
* Monad
* Semigroup

Reading the docs in EitherT.scala tells about the things mentioned above. But, we implicitly get more stuff depending on the type of F,A,B
Take this part with a grain of salt, and the special case of
+ F is always a Future
+ Always have an Either[OError,B]
+ OError doesn't currently have any special properties.
+ B can be anything, like Json with properties, but also have List[T] and Seq[T]



cats.instances.FutureInstances trait seem to give Future the following type attributes (if that is correct term)
(cats package object instances has all the Cats typelevel instances?)

- MonadError
- CoflatMap
- Monad
- 

