# README #

This is a scratchpad for GIST style reminders, examples and templates for Scala, SBT and in particular CATS / Circe and other goto libs.

### What is this repository for? ###

Essentially an extended set of GIST and notes for my  working with Cats and Scala.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


Feel free to issue pull requests.
