# Lots of Operations on EitherT style things. 
ANd most of them have cryptic U.M[U.A] style signatures. This is an exploration.


Againt we start with out monad transformer EitherT instead of building up.
And maybe that is special case

-- EitherT(Future[Either[OError,Json]]) == EitherT[Future,OError,Json]
-- EitherT[F,A,B]

Especially interested in cases like EitherT[Future,Error,List[B]]  when we have f1: B=>D or f2: B=> EitherT[F,A,D]

For coding and documenting, left  FErrorOr[B] type be EitherT[Future,OError,B] , F = Future, A = OError

## combine
If B is semigroup, e.g. List, then   x.combine(y) 


# map
takes f:B=>D, context is: EitherT[F,A,B] => EitherT[F,A,D]
## leftMap
leftMap takes f: A=> C, context is:  EitherT[F,A,B] => EitherT[F,C,B]

## flatMapF
flatMapF takes B => Future[Either[AA,D]].  Here AA is type of A  This is very handy if you keep your functions in Future[Either[A,B]] form.

## subflatMap
subflatMap is  B=> Either[AA,D]

## semiflatMap
semiflatMap is B => F[D], context is: EitherT[F,A,B] => EitherT[F,A,D]
Example:  EitherT[Future,OError,Json].semiflatMap(json=> doit(json))  where doit(x:Json): EitherT[Future,OError,Answer] 
Result:   EitherT[Future,OError,Json] goes to EitherT[Future,OError,Answer] iff first either is ok.


