# Standard Imports

```scala
import cats.syntax._
import cats.implicit._
import cats._
import cats.data._
```
Note some things (like some of the Monad Transformers) won't show up unless an execution context is in scope too.

Either has lots of helpers under implicits, fromOption fromTry etc...

```scala 
Either.fromOption(config.as[KeystoreConfig]("keystore"),"No Keystore Config Found")

val x: scala.util.Either = ???
x.leftMap(theLeftSide => Y)
```