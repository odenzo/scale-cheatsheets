 private case class KeystoreConfig(file_name: String,
                                    resource_name: String,
                                    keystore_password: String,
                                    key_password: String)

  val dynKeystore = {
    import net.ceedubs.ficus.Ficus._
    val config = ConfigHelpers.loadFromFactory("com.odenzo.tokenization.tokenizer")
    config.getAs("keystore")[KeystoreConfig]
  }