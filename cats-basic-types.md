# Basic Cats types and notes -- no direct applied.

Summary notes from      http://typelevel.org/cats/typeclasses.html


## Monoid 

Jjust needs to functions:

```scala
implicit val stringMonoid: Monoid[String] = new Monoid[String] {
  def empty: String = ""
  def combine(x: String, y: String): String = x ++ y
}
```
Thus a monoid for Money is trivial empty = XRP(0) and combine = a.drops + b.drops
Cannot have plus be a Tuple, since T != (T,T) shapes.

+ combine is an operator, and most be associate -- combine(a,b) == combine(b,a)
+ combine(a,empty) == a


## SemiGroup

```scala
implicit val intAdditionSemigroup: Semigroup[Int] = new Semigroup[Int] {
  def combine(x: Int, y: Int): Int = x + y
}
```
With law: combine(x, combine(y, z)) = combine(combine(x, y), z)

import cats.syntax.semigroup._
// import cats.syntax.semigroup._

1 |+| 2
// res5: Int = 3

Many semi-groups supplied, include SemiGroup[List[T]]  (combine two list not dependant on T obviously)
ValidatedNEL most used instance.

This is the Monoid for Option: for any Semigroup[A], there is a Monoid[Option[A]]
That handles SemiGroup with no "empty" like NonEmptyList


## Applicatives and Traversals

```scala
def traverseFuture[A, B](as: List[A])(f: A => Future[B])(implicit ec: ExecutionContext): Future[List[B]] =
  Future.traverse(as)(f)
```  

### Applicativate

Nice for doing things in parallel, few built in ones. Basically F[A], F[B] and f() = A=> B 
